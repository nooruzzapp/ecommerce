from django.contrib.auth.forms import AuthenticationForm
from django.views.generic.base import View
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, JsonResponse, Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic.detail import SingleObjectMixin, DetailView
from django.views.generic.edit import FormMixin

from orders.forms import GuestCheckoutForm
from products.models import Variation
from .models import Cart, CartItem
from orders.models import GuestCheckout

# Create your views here.

class CartViewCount(View):
	def get(self, request, *args, **kwargs):
		if request.is_ajax():
			cart_id = self.request.session.get('cart_id')
			if cart_id == None:
				count =0

			else:
				cart= Cart.objects.get(id=cart_id)
				count = cart.items.count()

			return JsonResponse({"count": count})
		else:
			raise Http404

class CartView(View):
	models = Cart
	template_name= "carts/views.html"
	def get_object(self, *args, **kwargs):
		cart_id = self.request.session.get('cart_id')
		if cart_id == None:
			cart = Cart()
			cart.save()
			cart_id = cart.id
			self.request.session['cart_id'] = cart_id
		cart = Cart.objects.get(id=cart_id)

		if self.request.user.is_authenticated():
			cart.user = self.request.user
			cart.save()
		return cart

	def get(self, request, *args, **kwargs):
		cart = self.get_object()

		
		item_id = request.GET.get('item')
		delete_item = request.GET.get('delete', False)
		item_added = False
		if item_id:
			item_instance = get_object_or_404(Variation, id=item_id)
			qty = request.GET.get('qty', 1)

			try:
				if int(qty) < 1:
					delete_item=True
			except:
				raise HttpResponseRedirect('/')
			cart_item, created = CartItem.objects.get_or_create(cart=cart, item=item_instance)
			if created:
				item_added = True
			if delete_item:
				cart_item.delete()
			else: 
				cart_item.quantity = qty
				cart_item.save()
				print cart_item
			if not request.is_ajax():
				return HttpResponseRedirect(reverse("cart"))


		if request.is_ajax():
			try:
				total = cart_item.line_item_total
			except:
				total = None
			try:
				subtotal= cart_item.cart.subtotal
			except:
				subtotal = None
			data={
			'deleted': delete_item, 
			'item_added': item_added,
			'line_item_total': total,
			'subtotal': subtotal, 



			}
			return JsonResponse(data)
		context ={
			"object" : self.get_object()
		}
		template = self.template_name
		return render(request, template, context)



class CheckoutView(FormMixin, DetailView):
	models = Cart
	template_name = "carts/checkout_view.html"
	form_class= GuestCheckoutForm
	def get_object(self, *args, **kwargs):
		cart_id = self.request.session.get('cart_id')
		if cart_id == None:
			return redirect ('cart')
		cart = Cart.objects.get(id=cart_id)
		return cart
	def get_context_data(self, *args, **kwargs):
		context = super(CheckoutView, self).get_context_data(*args, **kwargs)
		user_auth = False
		user_email_id = self.request.session.get('user_checkout_id')
		if not self.request.user.is_authenticated() or user_email_id==None:
			context['login_form']  = AuthenticationForm()
			context['next_url'] = self.request.build_absolute_uri()
		if self.request.user.is_authenticated() or user_email_id != None :
			user_auth = True

		if self.request.user.is_authenticated():
			user_email, created = GuestCheckout.objects.get_or_create(email=email)
			user_email.user = self.request.user
			user_email.save()
		context['user_auth'] = user_auth
		context['form']= self.get_form()
		return context

	def post(self, request, *args, **kwargs):
		form = self.get_form()
		self.object = self.get_object()
		if form.is_valid():
			email = form.cleaned_data.get('email')
			user_email, created = GuestCheckout.objects.get_or_create(email=email)
			request.session['user_checkout_id'] = user_email.id
			return self.form_valid(form)
		else:
			return self.form_invalid(form)
	def get_success_url(self):
		return reverse('checkout')











