from django import forms
from django.contrib.auth import get_user_model
User = get_user_model()

class GuestCheckoutForm(forms.Form):
	email = forms.EmailField()
	email2= forms.EmailField(label= 'Verify Email')

	def clean_email2(self):

		email = self.cleaned_data.get('email')
		email2 = self.cleaned_data.get('email2')
		if email == email2:
			user_exist = User.objects.filter(email=email).count()
			if user_exist != 0:
				raise forms.ValidationError("User Already Exist. Login Instead")

			return email2
		else:
			raise form.ValidationError("Please confirm the emails")