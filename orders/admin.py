from django.contrib import admin
from .models import GuestCheckout, UserAddress


admin.site.register(GuestCheckout)
admin.site.register(UserAddress)
# Register your models here.
