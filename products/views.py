from django.contrib import messages
from django.db.models import Q
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.http import Http404

# Create your views here.
from .forms import VariationInventoryFormSet

from .models import Product, Variation, Category
# from .mixins import StaffRequiredMixin

class CategoryListView(ListView):
	model = Category
	queryset = Category.objects.all()
	template_name= 'products/product_list.html'

class CategoryDetailView(DetailView):
	model = Category
	def get_context_data(self, *args, **kwargs):
		context = super(CategoryDetailView, self).get_context_data(*args, **kwargs)
		obj = self.get_object()
		product_set = obj.product_set.all()
		default_product = obj.default_category.all()
		products = (product_set | default_product).distinct()
		context['products'] = products
		return context



class ProductDetailView(DetailView):
	model = Product
	def get_context_data(self, *args, **kwargs):
		context = super(ProductDetailView, self).get_context_data(*args, **kwargs)
		context['related']= Product.objects.get_related(self.get_object())
		print context
		return context
	

class ProductListView(ListView):
	model = Product
	def get_context_data(self, *args, **kwargs):
		context = super(ProductListView, self).get_context_data(*args, **kwargs)
		print context
		return context

	def get_queryset(self, *args, **kwargs):
		qs = super(ProductListView, self).get_queryset(*args, **kwargs)
		query = self.request.GET.get("q")
		if query:
			qs = self.model.objects.filter(
				Q(title__icontains=query)|
				Q(description__icontains=query)

				)
		return qs

class VariationListView(ListView):
	model = Variation
	queryset= Variation.objects.all()
	def get_context_data(self, *args, **kwargs):
		context = super(VariationListView, self).get_context_data(*args, **kwargs)
		context['formset'] = VariationInventoryFormSet(queryset=self.get_queryset())
		return context


	def get_queryset(self, *args, **kwargs):
		qs = super(VariationListView, self).get_queryset(*args, **kwargs)
		products_pk = self.kwargs.get("pk")
		if products_pk:
			products = get_object_or_404(Product, pk=products_pk)
			queryset = Variation.objects.filter(product=products)
		return queryset
	def post(self, request, *args, **kwargs):
		formset = VariationInventoryFormSet(request.POST, request.FILES)

		if formset.is_valid():
			formset.save(commit=False)
			for form in formset:
				new_item = form.save(commit=False)
				if new_item.title:
					product_pk = self.kwargs.get("pk")
					product = get_object_or_404(Product, pk=product_pk)
					new_item.product = product
					new_item.save()
			messages.success(request, 'Form Successfully Submitted')
			return redirect('product')
		print request.POST
		raise Http404
# def product_detail_view_func(request, id):
# 	product_instance = get_object_or_404(Product, id=id)
# 	product_template= "products/product_detail.html"
# 	context = {
# 	"object": product_instance
# 	}
# 	return render(request, product_template, context)