from django.db import models
from django.core.urlresolvers import reverse
# Create your models here.
from django.db.models.signals import post_save
from django.template.defaultfilters import slugify
class ProductQuerySet(models.query.QuerySet):
	def active(self):
		return self.filter(active=True)


class ProductManager(models.Manager):
	def get_queryset(self):
		return ProductQuerySet(self.model, using=self._db)
	
	def all(self, *args, **kwargs):
		return self.get_queryset().active()

	def get_related(self, instance):
		product_one = self.get_queryset().filter(categories__in =instance.categories.all())
		return self.get_queryset()



class Product(models.Model):
	title = models.CharField(max_length=120)
	description= models.TextField(blank=True, null=True)
	price = models.DecimalField(decimal_places=2, max_digits=20)
	active = models.BooleanField(default=True)
	categories = models.ManyToManyField('Category', blank=True)
	default = models.ForeignKey('Category', related_name='default_category', null=True, blank=True)
	objects = ProductManager()

	def __unicode__(self):
		return self.title 

	def get_image_url(self):
		img = self.productimage_set.first()
		if img:
			return img.image.url
		return img #None

	def get_absolute_url(self):
		return reverse("product_detail", kwargs={"pk":self.pk})   

class Variation(models.Model):
	product = models.ForeignKey(Product)
	title = models.CharField(max_length=120)
	price = models.DecimalField(decimal_places=2, max_digits=20)
	sale_price = models.DecimalField(decimal_places=2, max_digits=20, null=True, blank=True)
	active = models.BooleanField(default=True)
	inventory = models.IntegerField(default="-1", null=True, blank=True)

	def __unicode__(self):
		return self.title

	def get_price(self):
		if self.sale_price is not None:
			return self.sale_price
		else:
			return self.price

	def get_absolute_url(self):
		return self.product.get_absolute_url()

	def add_to_cart(self):
		return "%s?item=%s&qty=1" %(reverse ('cart'), self.id)
	def remove_from_cart(self):
		return "%s?item=%s&qty=1&delete=True" %(reverse ('cart'), self.id)

	def get_title(self):
		return "%s- %s" %(self.title, self.product.title)




def product_saved_receiver(sender, instance, created, *args, **kwargs):
		product = instance
		variation = product.variation_set.all()
		if variation.count() == 0:
			new_var = Variation()
			new_var.product = product
			new_var.title = "Default"
			new_var.price = product.price
			new_var.save()

post_save.connect(product_saved_receiver, sender=Product)


def image_upload_to(instance, filename):
	title =instance.product.title
	slug = slugify(title)
	return "products/%s/%s"%(slug, filename)

class ProductImage(models.Model):
	product = models.ForeignKey(Product)
	image = models.ImageField(upload_to=image_upload_to)

	def __unicode__(self):
		return self.product.title


		
class Category(models.Model):
	title = models.CharField(max_length=120, unique=True)
	slug = models.SlugField(unique=True)
	description = models.TextField(null=True, blank=True)
	active = models.BooleanField(default=True)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
	def __unicode__(self):
		return self.title

	def get_absolute_url(self):
		return reverse('category_detail', kwargs={"slug": self.slug})




